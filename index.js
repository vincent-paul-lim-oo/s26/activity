const http = require('http')

const port = 3000

http.createServer((req,res)=> {
	if(req.url == "/login") {
		res.writeHead(200, {'Content-type' : 'text/plain'})
		res.end('Access the login route.')
	} 
	else {
		res.writeHead(400, {'Content-type' : 'text/plain'})
		res.end("Page not available")
	}

}).listen(port)

console.log("Server running on port " + port)
